package ilijavrbatovic.fesb.hr.finalapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;

public class StanoviAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;

    public StanoviAdapter(Context context) {
        mContext = context;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return DataStorageStanovi.stanovi.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        if (view == null)
        view = mInflater.inflate(R.layout.stanovi, parent, false);
        final ImageView imgView = (ImageView)view.findViewById(R.id.image_tmbstanovi);
        final TextView NaslovTV = (TextView) view.findViewById(R.id.txtnaslovoglas);
        final TextView OpisTV = (TextView) view.findViewById(R.id.txtopisoglas);
        final TextView LokacijaTV = (TextView) view.findViewById(R.id.txtlokacijaoglas);
        final TextView CijenaTV = (TextView) view.findViewById(R.id.txtcijenaoglas);
        final TextView ImeTV = (TextView) view.findViewById(R.id.txtimeoglas);
        final TextView PrezimeTV = (TextView) view.findViewById(R.id.txtprezimeoglas);
        final TextView KontaktTV = (TextView) view.findViewById(R.id.txtkontaktoglas);
        final TextView MailTV = (TextView) view.findViewById(R.id.txtmailoglas);
        final Spinner spinner = (Spinner)view.findViewById(R.id.filterspinner);


        final Stanovi stanovi = DataStorageStanovi.stanovi[position];

        Ion.with(imgView).load("http://www.industry4.com.hr/slst/pictures/" + stanovi.slika + ".jpg");
        NaslovTV.setText(stanovi.naslov + "\n");
        OpisTV.setText("Opis:" + "\n" + stanovi.opis + "\n");
        LokacijaTV.setText("Lokacija: " + stanovi.lokacija + "\n");
        CijenaTV.setText(stanovi.cijena + "\n" + "\n");
        ImeTV.setText("Korisnik: " + stanovi.ime);
        PrezimeTV.setText(stanovi.prezime + "\n");
        KontaktTV.setText("Kontakt: " + stanovi.kontakt + "\n");
        MailTV.setText("Mail: " + stanovi.mail + "\n");
        return view;
    }
}
