package ilijavrbatovic.fesb.hr.finalapp;

public class Stanovi {

    private	int	id;
    public String ime;
    public String prezime;
    public String kontakt;
    public String mail;
    public String naslov;
    public String opis;
    public String lokacija;
    public String cijena;
    public String slika;

    public Stanovi(){}

    public Stanovi(int id, String ime, String prezime, String kontakt, String mail,
                   String naslov, String opis, String lokacija, String cijena, String slika){
        this.id = id;
        this.ime= ime;
        this.prezime = prezime;
        this.kontakt = kontakt;
        this.mail = mail;
        this.naslov = naslov;
        this.opis = opis;
        this.lokacija = lokacija;
        this.cijena = cijena;
        this.slika = slika;
    }

    public Stanovi(String ime, String prezime, String kontakt, String mail,
                   String naslov, String opis, String lokacija, String cijena, String slika){
        this.ime= ime;
        this.prezime = prezime;
        this.kontakt = kontakt;
        this.mail = mail;
        this.naslov = naslov;
        this.opis = opis;
        this.lokacija = lokacija;
        this.cijena = cijena;
        this.slika = slika;
    }

    public int getId(){
        return this.id;
    }

    public void setId(int id){
        this.id = id;
    }

    public String getIme(){
        return this.ime;
    }

    public void setIme(String ime){
        this.ime = ime;
    }

    public String getPrezime(){
        return this.prezime;
    }

    public void setPrezime(String prezime){
        this.prezime = prezime;
    }

    public String getKontakt(){
        return this.kontakt;
    }

    public void setKontakt(String kontakt){
        this.kontakt = kontakt;
    }

    public String getMail(){
        return this.mail;
    }

    public void setMail(String mail){
        this.mail = mail;
    }

    public String getNaslov(){
        return this.naslov;
    }

    public void setNaslov(String naslov){
        this.naslov = naslov;
    }

    public String getOpis(){
        return this.opis;
    }

    public void setOpis(String opis){
        this.opis = opis;
    }

    public String getLokacija(){
        return this.lokacija;
    }

    public void setLokacija(String lokacija){
        this.lokacija = lokacija;
    }

    public String getCijena(){
        return this.cijena;
    }

    public void setCijena(String cijena){
        this.cijena = cijena;
    }

    public String getSlika(){
        return this.slika;
    }

    public void setSlika(String slika){
        this.slika = slika;
    }
}
