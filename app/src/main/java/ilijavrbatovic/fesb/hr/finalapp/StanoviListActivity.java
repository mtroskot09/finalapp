package ilijavrbatovic.fesb.hr.finalapp;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.MainThread;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;

public class StanoviListActivity extends MainActivity{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_stanovi_list);

        AdView mAdView = (AdView) findViewById(R.id.list_adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        ConnectivityManager cM = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cM.getActiveNetworkInfo();

        final Database db = new Database(this);
        Intent intent = getIntent();
        final String value = intent.getStringExtra("tablica");
        setTitle(value);

        final AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        final Gson mGson = new Gson();
        //final StanoviDataSource sds = new StanoviDataSource(this);

        ArrayAdapter<String> adapter;

        final ListView listViewStanovi1 = (ListView)findViewById(R.id.listViewStanovi);
        final Spinner spinner = (Spinner)findViewById(R.id.filterspinner);

        final RequestParams requestParams = new RequestParams();
        requestParams.add("tablica", value);

        if(networkInfo!=null && networkInfo.isConnected()) {
            asyncHttpClient.post("http://www.industry4.com.hr/slst/table_output.php", requestParams, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    Log.d("HTTP_RESPONSE", response.toString());

                    for (int i = 0; i < response.length(); i++) {
                        try {
                            JSONObject object = response.getJSONObject(i);
                            String ime = object.getString("ime");
                            String prezime = object.getString("prezime");
                            String kontakt = object.getString("kontakt");
                            String mail = object.getString("mail");
                            String naslov = object.getString("naslov");
                            String opis = object.getString("opis");
                            String lokacija = object.getString("lokacija");
                            String cijena = object.getString("cijena");
                            String slika = object.getString("sllika");
                            db.getAll();
                            Log.d("imetabliceje", value.toLowerCase());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    /*spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if(spinner.getSelectedItem().toString().equals("Kila"))
                            {
                                Intent myIntent = new Intent(StanoviListActivity.this, MainActivity.class);
                                startActivity(myIntent);
                            }
                            else if(spinner.getSelectedItem().toString().equals("Lokve"))
                            {
                                try {
                                    Toast.makeText(getApplicationContext(), sds.getAllNotes(value.toLowerCase()).toString(), Toast.LENGTH_LONG).show();
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });*/

                    DataStorageStanovi.stanovi = mGson.fromJson(response.toString(), Stanovi[].class);
                    listViewStanovi1.setAdapter(new StanoviAdapter(getApplicationContext()));
                    listViewStanovi1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent myIntent = new Intent(StanoviListActivity.this, DetailActivityStanovi.class);
                            myIntent.putExtra("key", String.valueOf(position));
                            myIntent.putExtra("ime", value);
                            StanoviListActivity.this.startActivity(myIntent);
                        }
                    });

                }
            });
        }
        else
        {
            Toast.makeText(getApplicationContext(),"Internet connection unavailable...",Toast.LENGTH_LONG).show();
        }
    }}
