package ilijavrbatovic.fesb.hr.finalapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.koushikdutta.ion.Ion;

public class DetailActivityStanovi extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_activity_stanovi);

        AdView mAdView = (AdView)findViewById(R.id.detail_adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        Intent intent = getIntent();
        String value = intent.getStringExtra("key");
        String ime = intent.getStringExtra("ime");
        setTitle(ime);

        final ImageView imageDS = (ImageView)findViewById(R.id.imagestanovidetail);
        final TextView imeDS = (TextView)findViewById((R.id.ImeOglasivacaStanoviDetail));
        final TextView prezimeDS = (TextView)findViewById((R.id.PrezimeOglasivacaStanoviDetail));
        final TextView kontaktDS = (TextView)findViewById(R.id.KontatOglasivacaStanoviDetail);
        final TextView mailDS = (TextView)findViewById((R.id.MailOglasivacaStanoviDetail));
        final TextView naslovDS = (TextView)findViewById((R.id.OglasNaslovStanoviDetail));
        final TextView opisDS = (TextView)findViewById((R.id.OglasOpisStanoviDetail));
        final TextView lokacijaDS = (TextView)findViewById(R.id.LokacijaOglasivacaStanoviDetail);
        final TextView cijenaDS = (TextView)findViewById(R.id.OglasCijenaStanoviDetail);
        final Button kontakt = (Button)findViewById(R.id.kontaktButton);
        final Button prikazi = (Button)findViewById(R.id.prikazinakarti);


        int pozicija = Integer.parseInt(value);

        final Stanovi stanoviDetail = DataStorageStanovi.stanovi[pozicija];

        Ion.with(imageDS).load("http://www.industry4.com.hr/slst/pictures/" + stanoviDetail.slika + ".jpg");
        imeDS.setText(" Ime: " + stanoviDetail.ime);
        prezimeDS.setText(" Prezime: " + stanoviDetail.prezime);
        kontaktDS.setText(" Kontakt: " + stanoviDetail.kontakt);
        mailDS.setText(" Mail" + stanoviDetail.mail);
        naslovDS.setText(stanoviDetail.naslov);
        opisDS.setText(" Opis:" + stanoviDetail.opis);
        lokacijaDS.setText(" Lokacija:" + stanoviDetail.lokacija);
        cijenaDS.setText(" Cijena:" + stanoviDetail.cijena);


        kontakt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent myIntent = new Intent(DetailActivityStanovi.this, KontaktActivity.class);
                        myIntent.putExtra("mail", stanoviDetail.mail);
                        startActivity(myIntent);
            }
        });

        /*prikazi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(DetailActivityStanovi.this, Karta.class);
                myIntent.putExtra("lokacija", stanoviDetail.lokacija);
                startActivity(myIntent);
            }
        });*/
    }
}
