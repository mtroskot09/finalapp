package ilijavrbatovic.fesb.hr.finalapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;

import java.io.ByteArrayOutputStream;

public class PredajOglasActivity extends MainActivity {

    Button B1, B2;
    EditText Ime, Prezime, Kontakt, Mail, Naslov, Opis, Cijena, Slika;
    String username, usersurname, usercontact, usermail, oglasnaslov, oglasopis, lokacija, cijena, imeslike;
    Spinner spinnerOglas, spinnerLokacija;

    private static final int RESULT_LOAD_IMAGE = 1;
    public ImageView IVupload, IVdownload;
    public String encodedImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_predaj_oglas);

        AdView mAdView = (AdView)findViewById(R.id.predaj_adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        spinnerOglas = (Spinner)findViewById(R.id.mySpinner);
        spinnerLokacija = (Spinner)findViewById(R.id.spinnerL);
        B1 = (Button)findViewById(R.id.buttonPredajOglas);
        B2 = (Button)findViewById(R.id.buttonDodajSliku);
        IVupload = (ImageView)findViewById(R.id.imageView);
        final EditText Ime = (EditText)findViewById(R.id.editTextIme);
        final EditText Prezime = (EditText)findViewById(R.id.editTextPrezime);
        final EditText Kontakt = (EditText)findViewById(R.id.editTextKontakt);
        final EditText Mail = (EditText)findViewById(R.id.editTextEmail);
        final EditText Naslov = (EditText)findViewById(R.id.editTextNaslovOglas);
        final EditText Opis = (EditText)findViewById(R.id.editTextOpisOglasa);
        final EditText Cijena = (EditText)findViewById(R.id.editTextCijena);
        final EditText Slika = (EditText)findViewById(R.id.editTextImeSlike);


        B1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                username = Ime.getText().toString();
                usersurname = Prezime.getText().toString();
                usercontact = Kontakt.getText().toString();
                usermail = Mail.getText().toString();
                oglasnaslov = Naslov.getText().toString();
                oglasopis = Opis.getText().toString();
                lokacija = spinnerLokacija.getSelectedItem().toString();
                cijena = Cijena.getText().toString();
                imeslike = Slika.getText().toString();

                if(IVupload.getDrawable()!=null){
                    Bitmap image = ((BitmapDrawable) IVupload.getDrawable()).getBitmap();

                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    image.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                    encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
                }
                else
                    encodedImage = "";

                AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
                RequestParams requestParams = new RequestParams();
                requestParams.add("bazaime", username);
                requestParams.add("bazaprezime", usersurname);
                requestParams.add("bazakontakt", usercontact);
                requestParams.add("bazamail",usermail);
                requestParams.add("bazanaslov", oglasnaslov);
                requestParams.add("bazaopis", oglasopis);
                requestParams.add("bazalokacija", lokacija);
                requestParams.add("bazacijena", cijena);
                requestParams.add("bazaslika",encodedImage + ".jpg");
                requestParams.add("tablica", spinnerOglas.getSelectedItem().toString());

                AsyncHttpClient upload = new AsyncHttpClient();
                RequestParams params = new RequestParams();
                params.add("name", imeslike);
                params.add("image", encodedImage);

                upload.post("http://213.147.114.178/ilija/slst/uploadslike.php", params, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int i, Header[] headers, byte[] bytes) {
                    }

                    @Override
                    public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                        Toast.makeText(getApplicationContext(), "Image upload error!",Toast.LENGTH_LONG).show();
                    }
                });

                asyncHttpClient.post("http://213.147.114.178/ilija/slst/table_input.php", requestParams, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int i, Header[] headers, byte[] bytes) {
                        Toast.makeText(getApplicationContext(), "Successfully added!", Toast.LENGTH_LONG).show();
                        Intent vrati = new Intent(PredajOglasActivity.this, MainActivity.class);
                        PredajOglasActivity.this.startActivity(vrati);
                    }

                    @Override
                    public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                        Toast.makeText(getApplicationContext(), "Upload error!", Toast.LENGTH_LONG).show();

                    }
                });

            }
        });

        B2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, RESULT_LOAD_IMAGE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data != null){

            Uri selectedImage = data.getData();
            IVupload.setImageURI(selectedImage);
        }
    }
}