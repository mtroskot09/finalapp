package ilijavrbatovic.fesb.hr.finalapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class KontaktActivity extends AppCompatActivity {

    String name, email, message,receipientMail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kontakt);

        AdView mAdView = (AdView)findViewById(R.id.kontakt_adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        final Intent intent = getIntent();
        final EditText ime = (EditText)findViewById(R.id.editTextIme);
        final EditText mail = (EditText)findViewById(R.id.editTextEmail);
        final EditText poruka = (EditText)findViewById(R.id.editTextPoruka);
        final Button posalji = (Button)findViewById(R.id.buttonPosalji);
        posalji.setEnabled(false);

        mail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!validateEmail(mail.getText().toString())) {
                    mail.setError("Invalid mail address");
                }
                else {
                    posalji.setEnabled(true);
                }
            }
        });

        posalji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                name = ime.getText().toString();
                email = mail.getText().toString();
                message = poruka.getText().toString();
                receipientMail = intent.getStringExtra("mail");

                AsyncHttpClient contact = new AsyncHttpClient();
                final RequestParams contactinfo = new RequestParams();
                contactinfo.add("contactname", name);
                contactinfo.add("contactmail", email);
                contactinfo.add("contactmessage", message);
                contactinfo.add("contactreceipientmail", receipientMail);

                contact.post("http://213.147.114.178/ilija/slst/contactest.php", contactinfo, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int i, Header[] headers, byte[] bytes) {
                        Toast.makeText(getApplicationContext(), "Message sent", Toast.LENGTH_LONG).show();
                        Toast.makeText(getApplicationContext(),contactinfo.toString(),Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                        Toast.makeText(getApplicationContext(), "Communication error", Toast.LENGTH_LONG).show();
                    }
                });

            }
        });
    }

    protected boolean validateEmail(String s) {
        Pattern pattern = Pattern.compile("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                "\\@" +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                "(" +
                "\\." +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                ")+");
        Matcher matcher = pattern.matcher(s);
        return matcher.matches();
    }
}
