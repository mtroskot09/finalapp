package ilijavrbatovic.fesb.hr.finalapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.koushikdutta.ion.builder.Builders;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("ALL")
public class OglasRecyclerView extends MainActivity {
    private List<Oglas> oglasList = new ArrayList<>();
    private RecyclerView recyclerView;
    private OglasAdapter mAdapter;
    private Database db;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recyclerview_layout);

        ConnectivityManager cM = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cM.getActiveNetworkInfo();

        if(networkInfo!=null && networkInfo.isConnected())
        {
            dbDownload();

        }
        else
        {
            Toast.makeText(getApplicationContext(),"Internet connection unavailable...",Toast.LENGTH_LONG).show();

        }

        Intent intent = getIntent();
        final String value = intent.getStringExtra("tablica");
        setTitle(value);

        final AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        final Gson mGson = new Gson();
        final RecyclerView recyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        Button test = (Button)findViewById(R.id.test);
        final RequestParams requestParams = new RequestParams();
        requestParams.add("tablica", value);

        //oglasList = db.getOglas();
        //mAdapter = new OglasAdapter(oglasList);
        //mAdapter = new OglasAdapter(db.getOglas());

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        if(networkInfo != null && networkInfo.isConnected()){
            asyncHttpClient.post("http://213.147.114.178/ilija/slst/table_output.php", requestParams, new JsonHttpResponseHandler(){
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                                    //super.onSuccess(statusCode, headers, response);
                                    Log.d("HTTP_RESPONSE", response.toString());
                                    //db.getOglas();

                                    for (int i = 0; i < response.length(); i++){
                                        try {
                                            JSONObject object = response.getJSONObject(i);
                                            String Ime = object.getString("Ime");
                                            String Prezime = object.getString("Prezime");
                                            String Kontakt = object.getString("Kontakt");
                                            String Mail = object.getString("Mail");
                                            String Naslov = object.getString("Naslov");
                                            String Opis = object.getString("Opis");
                                            String Lokacija = object.getString("Lokacija");
                                            String Cijena = object.getString("Cijena");
                                            String Slika = object.getString("Slika");
                                            Log.d("Ime tablice je", value);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    DataStorageOglas.oglasi = mGson.fromJson(response.toString(), Oglas[].class);
                                    recyclerView.setAdapter(new OglasAdapter(getApplicationContext()));
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                }
            });
        }

        else
        {
            Toast.makeText(getApplicationContext(), "Internet connection unavailable...", Toast.LENGTH_LONG).show();
        }



        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent myIntent = new Intent(OglasRecyclerView.this, DetailOglas.class);
                myIntent.putExtra("key", String.valueOf(position));
                myIntent.putExtra("ime", value);
                OglasRecyclerView.this.startActivity(myIntent);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener{

        private GestureDetector gestureDetector;
        private OglasRecyclerView.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final OglasRecyclerView.ClickListener clickListener){
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener(){
                @Override
                public boolean onSingleTapUp (MotionEvent e){
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e){
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if(child != null && clickListener != null){
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent (RecyclerView rv, MotionEvent e){

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)){
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    private void dbDownload() {
        try{
            URL url = new URL("http://213.147.114.178/ilija/slst/slst.sql");

            String dbPath = getFilesDir().getPath();
            dbPath = dbPath.substring(0, dbPath.lastIndexOf("/"));
            String storageLocation = dbPath + "/databases/slst.sql";

            SharedPreferences prefs = getSharedPreferences(SQLITE_LAST_UPDATE, 0);
            long defValue = Long.MIN_VALUE;
            long lastUpdate = prefs.getLong("lastUpdateTime", defValue);

            HttpFileDownloader.DownloadFile(
                    new HttpFileDownloader.DownloadStatusListener() {
                        @Override
                        public void OnDownloadDone() {

                            Long timeStampLong = System.currentTimeMillis();
                            SharedPreferences prefs = getSharedPreferences(SQLITE_LAST_UPDATE, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putLong("lastUpdateTime", timeStampLong);
                            editor.apply();
                            Toast.makeText(getApplicationContext(),"Download ok...",Toast.LENGTH_LONG).show();

                        }

                        @Override
                        public void OnError(String filename, final Exception exception, String error) {
                            Toast.makeText(getApplicationContext(),"Download failed...",Toast.LENGTH_LONG).show();

                        }

                        @Override
                        public void OnProgress(HttpFileDownloader.DownloadProgress downloadProgress) {

                        }
                    }, url, storageLocation, lastUpdate
            );

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

