package ilijavrbatovic.fesb.hr.finalapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;

public class DetailOglas extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detailoglas_activity);

        Intent intent = getIntent();
        String value = intent.getStringExtra("key");
        String ime = intent.getStringExtra("ime");
        setTitle(ime);

        final ImageView imageDetail = (ImageView)findViewById(R.id.image_detail);
        final TextView imeDetail = (TextView)findViewById(R.id.ime_detail);
        final TextView prezimeDetail = (TextView)findViewById(R.id.prezime_detail);
        final TextView kontaktDetail = (TextView)findViewById(R.id.kontakt_detail);
        final TextView mailDetail = (TextView)findViewById(R.id.mail_detail);
        final TextView naslovDetail = (TextView)findViewById(R.id.naslov_detail);
        final TextView opisDetail = (TextView)findViewById(R.id.opis_detail);
        final TextView lokacijaDetail = (TextView)findViewById(R.id.lokacija_detail);
        final TextView cijenaDetail = (TextView)findViewById(R.id.cijena_detail);
        final Button kontaktBnt = (Button)findViewById(R.id.kontaktButton);
        final Button prikazi = (Button)findViewById(R.id.prikazinakarti);

        int pozicija = Integer.parseInt(value);

        final Oglas oglasi = DataStorageOglas.oglasi[pozicija];

        Ion.with(imageDetail).load("http://213.147.114.178/ilija/slst/pictures/" + oglasi.Slika + "jpg");
        imeDetail.setText("Ime: " + oglasi.Ime);
        prezimeDetail.setText("Prezime: " + oglasi.Prezime);
        kontaktDetail.setText("Kontakt: " + oglasi.Kontakt);
        mailDetail.setText("Mail: " + oglasi.Mail);
        naslovDetail.setText("Naslov: " + oglasi.Naslov);
        opisDetail.setText("Opis: " + oglasi.Opis);
        lokacijaDetail.setText("Lokacija: " + oglasi.Lokacija);
        cijenaDetail.setText("Cijena: " + oglasi.Cijena);

        kontaktBnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(DetailOglas.this, KontaktActivity.class);
                //myIntent.putExtra("mail", );
                startActivity(myIntent);
            }
        });

    }
}
