package ilijavrbatovic.fesb.hr.finalapp;

import android.app.ActionBar;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.provider.BaseColumns;
import android.util.Log;

import com.koushikdutta.async.AsyncServer;
import com.koushikdutta.async.http.AsyncHttpClient;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Database extends SQLiteOpenHelper{

    private static final String TABLE_STANOVI = "Stanovi";
    private static final String TABLE_DOMOVI = "domovi";
    private static final String TABLE_CIMERI = "cimeri";
    private static final String TABLE_KNJIGE = "knjige";
    private static final String TABLE_INSTRUKCIJE = "instrukcije";
    private static final String TABLE_ZABAVA = "Zabava";

    private static final String KEY_IME = "Ime";
    private static final String KEY_PREZIME = "Prezime";
    private static final String KEY_KONTAKT = "Kontakt";
    private static final String KEY_MAIL = "Mail";
    private static final String KEY_NASLOV = "Naslov";
    private static final String KEY_OPIS = "Opis";
    private static final String KEY_LOKACIJA = "Lokacija";
    private static final String KEY_CIJENA = "Cijena";
    private static final String KEY_SLIKA = "Slika";

    private static final String[] COLUMNS = {KEY_IME, KEY_PREZIME, KEY_KONTAKT, KEY_MAIL, KEY_NASLOV, KEY_OPIS, KEY_LOKACIJA, KEY_CIJENA, KEY_SLIKA};

    private boolean mInvalidDatabaseFile = false;
    private static File DATABASE_FILE;
    private Context mContext;

    private static final String DATABASE_NAME = "slst.sql";
    private static final int DATABASE_VERSION = 1;

    public Database(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.mContext = context;

        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

            if (db != null){
                db.close();
            }

            DATABASE_FILE = context.getDatabasePath(DATABASE_NAME);

            if (mInvalidDatabaseFile){
                initDatabase();
            }

        } catch (SQLiteException e){
        } finally {
            if (db != null && db.isOpen()){
                db.close();
            }
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        mInvalidDatabaseFile = true;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        mInvalidDatabaseFile = true;
    }

    private void initDatabase() {
        AssetManager assetManager = mContext.getResources().getAssets();
        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            inputStream = assetManager.open("databases/" + DATABASE_NAME);
            outputStream = new FileOutputStream(DATABASE_FILE);
            byte[] buffer = new byte[1024];
            int read = 0;
            while ((read = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, read);
            }
        } catch (IOException e){
        } finally {
            if (inputStream != null){
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }
            if (outputStream != null){
                try {
                    outputStream.close();
                } catch (IOException e) {
                }
            }
        }
    }

    public List<Oglas> getOglas(){
        List<Oglas> data = new ArrayList<>();
        Oglas oglasi = null;
        Cursor cursor = getAll();
        if (cursor != null){
            /*oglasi = new Oglas();
            oglasi.setIme("null");
            data.add(oglasi);*/
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()){
                int ImeINT = cursor.getColumnIndex("Ime");
                String imeS = cursor.getString(ImeINT);
                int PrezimeINT = cursor.getColumnIndex("Prezime");
                String prezimeS = cursor.getString(PrezimeINT);
                int KontaktINT = cursor.getColumnIndex("Kontakt");
                String kontaktS = cursor.getString(KontaktINT);
                int MailINT = cursor.getColumnIndex("Mail");
                String mailS = cursor.getString(MailINT);
                int NaslovINT = cursor.getColumnIndex("Naslov");
                String naslovS = cursor.getString(NaslovINT);
                int OpisINT = cursor.getColumnIndex("Opis");
                String opisS = cursor.getString(OpisINT);
                int LokacijaINT = cursor.getColumnIndex("Lokacija");
                String lokacijaS = cursor.getString(LokacijaINT);
                int CijenaINT = cursor.getColumnIndex("Cijena");
                String cijenaS = cursor.getString(CijenaINT);
                int SlikaINT = cursor.getColumnIndex("Slika");
                String slikaS = cursor.getString(SlikaINT);
                oglasi = new Oglas();
                oglasi.setIme(imeS);
                oglasi.setPrezime(prezimeS);
                oglasi.setKontakt(kontaktS);
                oglasi.setMail(mailS);
                oglasi.setNaslov(naslovS);
                oglasi.setOpis(opisS);
                oglasi.setLokacija(lokacijaS);
                oglasi.setCijena(cijenaS);
                oglasi.setSlika(slikaS);
                data.add(oglasi);
            }
            /*oglasi = new Oglas();
            oglasi.setIme("null");
            data.add(oglasi);*/
        }
        assert cursor != null;
        cursor.close();
        return data;
    }


    public Cursor getAll(){

        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        String [] sqlSelect = {"Ime", "Prezime", "Kontakt", "Mail", "Naslov", "Opis", "Lokacija", "Cijena", "Slika"};

        qb.setTables(TABLE_ZABAVA);
        Cursor cursor = qb.query(db, sqlSelect, null, null, null, null, null);

        cursor.moveToFirst();
        return cursor;
    }


    public List<Oglas> getStanovi(){
        List<Oglas> oglasList = new ArrayList<Oglas>();

        String selectQuery = "SELECT * FROM " + TABLE_STANOVI;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()){
            do {
                Oglas oglas = new Oglas();
                oglas.setIme(cursor.getString(0));
                oglas.setPrezime(cursor.getString(1));
                oglas.setKontakt(cursor.getString(2));
                oglas.setMail(cursor.getString(3));
                oglas.setNaslov(cursor.getString(4));
                oglas.setOpis(cursor.getString(5));
                oglas.setLokacija(cursor.getString(6));
                oglas.setCijena(cursor.getString(7));
                oglas.setSlika(cursor.getString(8));
                oglasList.add(oglas);
            }while (cursor.moveToNext());
        }

        return oglasList;
    }
}