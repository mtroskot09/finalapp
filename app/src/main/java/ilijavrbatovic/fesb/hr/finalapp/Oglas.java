package ilijavrbatovic.fesb.hr.finalapp;

public class Oglas {
    public String Ime, Prezime, Kontakt, Mail, Naslov, Opis, Lokacija, Cijena, Slika;

    public Oglas(){
    }

    public Oglas(String Ime, String Prezime, String Kontakt, String Mail, String Naslov, String Opis, String Lokacija, String Cijena, String Slika){
        this.Ime = Ime;
        this.Prezime = Prezime;
        this.Kontakt = Kontakt;
        this.Mail = Mail;
        this.Naslov = Naslov;
        this.Opis = Opis;
        this.Lokacija = Lokacija;
        this.Cijena = Cijena;
        this.Slika = Slika;
    }

    public String getIme(){
        return Ime;
    }

    public void setIme(String Ime){
        this.Ime = Ime;
    }

    public String getPrezime(){
        return Prezime;
    }

    public void setPrezime(String Prezime){
        this.Prezime = Prezime;
    }

    public String getKontakt(){
        return Kontakt;
    }

    public void setKontakt(String Kontakt){
        this.Kontakt = Kontakt;
    }

    public String getMail(){
        return Mail;
    }

    public void setMail(String Mail){
        this.Mail = Mail;
    }

    public String getNaslov(){
        return Naslov;
    }

    public void setNaslov(String Naslov){
        this.Naslov = Naslov;
    }

    public String getOpis(){
        return Opis;
    }

    public void setOpis(String Opis){
        this.Opis = Opis;
    }

    public String getLokacija(){
        return Lokacija;
    }

    public void setLokacija(String Lokacija){
        this.Lokacija = Lokacija;
    }

    public String getCijena(){
        return Cijena;
    }

    public void setCijena(String Cijena){
        this.Cijena = Cijena;
    }

    public String getSlika(){
        return Slika;
    }

    public void setSlika(String Slika){
        this.Slika = Slika;
    }
}
