package ilijavrbatovic.fesb.hr.finalapp;

import android.content.Context;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import java.util.List;

public class OglasAdapter extends RecyclerView.Adapter<OglasAdapter.MyViewHolder>{

    private Context mContext;
    private LayoutInflater mInflater;

    public OglasAdapter(Context context){
        mContext = context;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public List<Oglas> oglasList;

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView Naslov, Opis, Lokacija, Cijena, Ime, Prezime, Kontakt, Mail;
        public ImageView Slika;

        public MyViewHolder(View view){
            super(view);
            Slika = (ImageView) view.findViewById(R.id.imageoglas);
            Naslov = (TextView) view.findViewById(R.id.txtnaslovoglas);
            Opis = (TextView) view.findViewById(R.id.txtopisoglas);
            Lokacija = (TextView) view.findViewById(R.id.txtlokacijaoglas);
            Cijena = (TextView) view.findViewById(R.id.txtcijenaoglas);
            Ime = (TextView) view.findViewById(R.id.txtimeoglas);
            Prezime = (TextView) view.findViewById(R.id.txtprezimeoglas);
            Kontakt = (TextView) view.findViewById(R.id.txtkontaktoglas);
            Mail = (TextView) view.findViewById(R.id.txtmailoglas);
        }
    }

    public OglasAdapter(List<Oglas> oglasList){
        this.oglasList = oglasList;
    }

    @Override
    public OglasAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(OglasAdapter.MyViewHolder holder, int position) {
        Oglas oglas = oglasList.get(position);
        holder.Slika.setImageURI(Uri.parse("http://213.147.114.178/ilija/slst/pictures" + oglas.Slika + ".jpg"));
        holder.Naslov.setText(oglas.getNaslov());
        holder.Opis.setText(oglas.getOpis());
        holder.Lokacija.setText(oglas.getLokacija());
        holder.Cijena.setText(oglas.getCijena());
        holder.Ime.setText(oglas.getIme());
        holder.Prezime.setText(oglas.getPrezime());
        holder.Kontakt.setText(oglas.getKontakt());
        holder.Mail.setText(oglas.getMail());

    }

    @Override
    public int getItemCount() {
        return 0;
        //return oglasList.size();
    }

}
