package ilijavrbatovic.fesb.hr.finalapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.webkit.DownloadListener;
import android.widget.Button;
import android.widget.Toast;
import android.os.Handler;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.koushikdutta.async.AsyncServer;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.LogRecord;

public class MainActivity extends AppCompatActivity {

    public static final String SQLITE_LAST_UPDATE = "SQLiteLastUpdate";
    SharedPreferences prefs = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AdView mAdView = (AdView) findViewById(R.id.main_adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        ConnectivityManager cM = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cM.getActiveNetworkInfo();

        if(networkInfo!=null && networkInfo.isConnected())
        {
            //dbDownload();
        }
        else
        {
            Toast.makeText(getApplicationContext(),"Internet connection unavailable...",Toast.LENGTH_LONG).show();

        }

        Button stanoviButton = (Button)findViewById(R.id.stanovi);
        Button domoviButton = (Button)findViewById(R.id.domovi);
        Button cimeriButton = (Button)findViewById(R.id.cimeri);
        Button knjigeButton = (Button)findViewById(R.id.knjige);
        Button instrukcijeButton = (Button)findViewById(R.id.instrukcije);
        Button zabavaButton = (Button)findViewById(R.id.zabava);
        Button kartaButton = (Button)findViewById(R.id.karta);
        final Button predaj_main = (Button)findViewById(R.id.predaj_main);

        stanoviButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MainActivity.this, OglasRecyclerView.class);
                myIntent.putExtra("tablica","Stanovi");
                startActivity(myIntent);
            }
        });

        domoviButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MainActivity.this, OglasRecyclerView.class);
                myIntent.putExtra("tablica","Domovi");
                startActivity(myIntent);
            }
        });

        cimeriButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MainActivity.this, OglasRecyclerView.class);
                myIntent.putExtra("tablica","Cimeri");
                startActivity(myIntent);
            }
        });

        knjigeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MainActivity.this, OglasRecyclerView.class);
                myIntent.putExtra("tablica","Knjige");
                startActivity(myIntent);
            }
        });

        instrukcijeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MainActivity.this, OglasRecyclerView.class);
                myIntent.putExtra("tablica","Instrukcije");
                startActivity(myIntent);
            }
        });

        zabavaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MainActivity.this, OglasRecyclerView.class);
                myIntent.putExtra("tablica","Zabava");
                startActivity(myIntent);
            }
        });


        predaj_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MainActivity.this,PredajOglasActivity.class);
                startActivity(myIntent);
            }
        });
        /*kartaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent (MainActivity.this, Karta.class);
                startActivity(myIntent);
            }
        });*/

    }

}











